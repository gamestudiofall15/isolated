﻿using UnityEngine;
using System.Collections;

public class Dataless : MonoBehaviour {

	// Array that defines Track
	public Transform[] trackNodes;
	
	// Dataless movement speed
	public float speed;

	// Container for the current Track Node that Datalles is moving towards to ( trackNodes[0] -> first trackNode, trackNodes[1] -> escond trackNode, etc)
	private int currentNode;

	public Light enemylight;

	// Use this for initialization
	void Start () {
		// Datalees start at first Track
		this.transform.position = trackNodes [0].position;
		// Dataless want to move to the next node
		currentNode = 1;
	}
	
	// Update is called once per frame
	void Update () {
		// Changes Dataless  position to make it closer to target trackNode every frame
		this.transform.position = Vector3.MoveTowards (this.transform.position, trackNodes [currentNode].position, Time.deltaTime * speed);

		// If i got to my Target Node, the next one will be my target
		if (this.transform.position == trackNodes [currentNode].position) {
			//If the last target was the last on the array, go to the first one (See module(%) operand)
			currentNode = (currentNode + 1) % trackNodes.Length;
		}


	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			enemylight.color = Color.red; 
		}
	}
	/*
	 *  iGNORE THIS FOR NOW
	void OnTriggerEnter (Collider other) {
		Debug.Log(other.gameObject.tag);
		if (other.gameObject.tag == "TrackNode") {
			Debug.Log(currentNode);
			currentNode = (currentNode + 1) % trackNodes.Length;
			Debug.Log(currentNode);
		}
	}
	//*/
}
