﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    private Rigidbody rb;
    public float jumpSpeed;
	public float secondJumpSpeed;
	public float regularSpeed;
	public float runSpeed;
	public float crouchSpeed;
	public float slideSpeed;
	public float slideDuration;
	public float slideReqVelocity;


	public float jumpDealey;	
	//public float rayCastDistance;

	//public Transform RayCastOrigin1, RayCastOrigin2;
	public GameObject landingCollider;

	private float speed;
	private float currentVelocity;

    private bool grounded = true;
	private bool secondJump;
	private bool canSlide;
	private bool isAtWall;

	private int verDirection;
	private int horDirection;
	private int otherPosition;
	 
	private Vector3 regularScale;
	private Vector3 crouchScale;
	private Vector3 slideScale;

	private Vector3 lastPosition;

	private PlayerStates playerState;

	public enum PlayerStates {
		Idle,
		Walk,
		Run,
		Jump,
		Climb,
		Slide,
		Crouch,
	};

    void Start () {
		playerState = PlayerStates.Idle;
        rb = GetComponent<Rigidbody>();
		speed = regularSpeed;
		horDirection = 1;
		verDirection = 1;
		isAtWall = false;

		regularScale = transform.localScale;
		crouchScale = transform.localScale - new Vector3(0, 0.5f, 0);
		slideScale = transform.localScale - new Vector3(-0.3f, 0.3f, 0);
		landingCollider.GetComponent<BoxCollider> ().enabled = false;
		lastPosition = this.transform.position;
	}

    void Update()
    {

		if (Input.GetAxisRaw ("Horizontal") > 0) {
			horDirection = 1;
		} 
		else if (Input.GetAxisRaw ("Horizontal") < 0) {
			horDirection = -1;
		}

		if (Input.GetAxisRaw ("Vertical") >= 0) {
			verDirection = 1;
		} 
		else if (Input.GetAxisRaw ("Vertical") < 0) {
			verDirection = -1;
		}



		switch (playerState) {
		case PlayerStates.Idle:
			IdleUpdate ();
			break;
		case PlayerStates.Walk:
			WalkUpdate ();
			break;
		case PlayerStates.Run:
			RunUpdate ();
			break;
		case PlayerStates.Jump:
			JumpUpdate ();
			break;
		case PlayerStates.Climb:
			ClimbUpdate ();
			break;
		case PlayerStates.Slide:
			SlideUpdate ();
			break;
		case PlayerStates.Crouch:
			CrouchUpdate ();
			break;
		default:
			break;
		}

		currentVelocity = this.transform.position.x - lastPosition.x;

		lastPosition = this.transform.position;

    }
	
    void OnCollisionEnter(Collision collisionInfo)
    {
		if (collisionInfo.gameObject.tag == "Wall") {
			isAtWall = true;

			if( collisionInfo.gameObject.transform.position.x > this.gameObject.transform.position.x) {
				otherPosition = -1;
			}
			else {
				otherPosition = 1;
			}
		}
    }

	void OnCollisionExit(Collision collisionInfo)
	{
		if (collisionInfo.gameObject.tag == "Wall") {
			isAtWall = false;
			otherPosition = 0;
		}
	}

	private void IdleUpdate() {
		if (Input.GetKeyDown(KeyCode.Space) && grounded == true)
		{
			if(verDirection == 1) {
				Jump (); 
			}
			else if (verDirection == -1) {
				Slide ();
			}
		}

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Run ();
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			Crouch ();
		}

		if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			StopCrouch();
		}
	}

	private void WalkUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)  && grounded == true)
		{
			if(verDirection == 1) {
				Jump (); 
			}
			else if (verDirection == -1) {
				Slide ();
			}
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			Run ();
		}

		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			Crouch ();
		}

		if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			StopCrouch();
		}

		if (Input.GetAxisRaw ("Horizontal") == 0) 
		{
			playerState = PlayerStates.Idle;
		}

	}

	private void RunUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)  && grounded == true)
		{
			if(verDirection == 1) {
				Jump (); 
			}
			else if (verDirection == -1) {
				Slide ();
			}
		}	

		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			StopRun ();
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

	}

	private void JumpUpdate() {
		if (Input.GetKeyDown(KeyCode.Space) && secondJump == false)
		{
			SecondJump (); 
		}

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}
	}

	private void ClimbUpdate() {

		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}
	}

	private void SlideUpdate() {

	}

	private void CrouchUpdate() {
		if (Input.GetAxisRaw ("Horizontal") != 0) 
		{
			Walk ();
		}

		if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			StopCrouch();
		}
	}

	private void Jump() {
		Debug.Log ("Jump!");
		rb.AddForce(0, jumpSpeed, 0);
		grounded = false;
		playerState = PlayerStates.Jump;
		secondJump = false;
		Invoke ("ActiveLandingCollider", 0.2f);
	}

	private void SecondJump() {
		Debug.Log ("SecondJump!");

		if (!isAtWall) {
			rb.AddForce((Input.GetAxis("Horizontal") * secondJumpSpeed/8), secondJumpSpeed, 0);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			rb.AddForce((Input.GetAxis("Horizontal") * secondJumpSpeed/8), secondJumpSpeed, 0);
		}

		secondJump = true;
	}

	private void Walk() {
		if (!isAtWall) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		}

		if ((playerState != PlayerStates.Jump) && (playerState != PlayerStates.Crouch) && (playerState != PlayerStates.Run) && (playerState != PlayerStates.Slide)) {
			playerState = PlayerStates.Walk;
		}
	}

	private void Run() {
		speed = runSpeed;

		if (!isAtWall) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			transform.Translate (speed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, 0f);
		}

		playerState = PlayerStates.Run;
	}

	private void StopRun() {
		speed = regularSpeed;
		playerState = PlayerStates.Walk;
	}

	private void Crouch() {
		playerState = PlayerStates.Crouch;
		transform.localScale = crouchScale;
		speed = crouchSpeed;
	}

	private void StopCrouch() {
		transform.localScale = regularScale;
		speed = regularSpeed;

		if(Input.GetAxisRaw ("Horizontal") != 0) {
			playerState = PlayerStates.Walk;
		}
		else {
			playerState = PlayerStates.Idle;
		}

	}

	private void Slide() {
		if (!isAtWall) {
			rb.AddForce (slideSpeed * horDirection, 0, 0);
		} 
		else if (isAtWall && horDirection == otherPosition) {
			rb.AddForce (slideSpeed * horDirection, 0, 0);
		}

		rb.AddForce (slideSpeed * horDirection, 0, 0);
		Debug.Log (slideSpeed * horDirection);
		playerState = PlayerStates.Slide;
		transform.localScale = slideScale;
		Invoke ("StopSlide", slideDuration);
	}


	private void StopSlide () {
		playerState = PlayerStates.Idle;
		transform.localScale = regularScale;
	}

	public void Landing() {
		grounded = true;
		landingCollider.GetComponent<BoxCollider> ().enabled = false;
		if (playerState == PlayerStates.Jump) { 
			playerState = PlayerStates.Idle;
		}
	}

	public void ActiveLandingCollider() {
		landingCollider.GetComponent<BoxCollider> ().enabled = true;
	}

}