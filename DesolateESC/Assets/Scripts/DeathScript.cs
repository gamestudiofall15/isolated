﻿using UnityEngine;
using System.Collections;

public class DeathScript : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);

        }

    }
}
