﻿using UnityEngine;
using System.Collections;

public class Push : MonoBehaviour {


    public float pushPower = 2.0f;

    private Rigidbody rb;

    void Start()
    {

        rb = GetComponent<Rigidbody>();

    }





    void OnControllerColliderHit(ControllerColliderHit hit)
    {

        rb = hit.collider.attachedRigidbody;
        if (rb == null || rb.isKinematic)
            return;

        if (hit.moveDirection.y < -0.3f)
            return;

        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
        rb.velocity = pushDir * pushPower;


    }

}
